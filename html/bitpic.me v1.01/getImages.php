<?
	//PHP SCRIPT: getImages.php
	Header("content-type: application/x-javascript");
	
	//This function gets the file names of all images in the current directory
	//and ouputs them as a JavaScript array
	function returnImages($dirname = ".") {
		$pattern = "(\.jpg$)|(\.png$)|(\.jpeg$)|(\.gif$)";			//valid image extensions
		$files = array();
		$current = 0;
		
		if ($handle = opendir($dirname)) {
			while (false !== ($file = readdir($handle))) {
				if (eregi($pattern, $file)) { 						//if this file is a valid image
					//Output it as a JavaScript array element
					echo 'uploadsArray['.$current.']="'.$file .'";';
					$current++;
				}
			}
			closedir($handle);
		}
		return($files);
	}
	
	echo 'var uploadsArray = new Array();'; 						//Define array in JavaScript
	returnImages();													//Output the array elements containing the image file names
?> 